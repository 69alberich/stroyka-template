var dict = {
    "You entered incorrect email or password.": {
        en: "You entered incorrect email or password.",
        es: "Usuario o contraseña incorrectos."
    },
    "is temporarily blocked":{
        en: "Your user is temporarily blocked, recover you password",
        es: "Tu usuario esta bloqueado temporalmente, reestablece tu contraseña"
    },
    "busy": {
        en: "This email is already busy",
        es: "El correo electronico ya esta utilizado",
    },
    "The Password confirmation does not match.":{
        en: "The Password confirmation does not match.",
        es: "Las contraseñas no coinciden entre sí",
    },
    "You have been successfully authorized.":{
        en: "You have been successfully authorized.",
        es: "Bienvenido",
    },
    "Old password entered incorrectly.":{
        en: "Old password entered incorrectly.",
        es: "tu contraseña anterior no coincide",
    }
};