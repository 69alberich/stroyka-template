$("document").ready(function(){
    
    var translator = $('body').translate({lang: "es", t: dict});
    
    $("body").on("submit", ".login-form", function(e){
        e.preventDefault();

        $(this).request('Login::onAjax', {
            'update': {
                'desktop-header': '#desktop-header-container',
                'register-form': '#register-form-container',
                'cart/cart-wizard': '#cart-wizard-container',
                'mobile-header': '#mobile-header-container',
            },
            success: function(data){
                if(data.status == false){
                    toastr.warning(translator.get(data.message), 'Espera' );
                }else{
                    //header.js
                    this.success(data);
                    toastr.success("Bienvenido", '¡HOLA!' );
                    initHeader();
                    initWizard();
                    updateCollapses();
                }

            },
            
            error: function(data){  
                toastr.error(translator.get(data.message), 'Ha ocurrido un error' );
            }
        });
    });

    $("#register-form-container").on("submit", "#register-form", function(e){
        e.preventDefault();
        var form = $(this).serializeArray();
        //console.log(data);
        var data = convertToObject(form);
        var message = validateRegister(data); 

        if(message !==""){
            toastr.warning(message, 'Espera' );
           }else{
           
            var document_number = data["property[document_type]"]+"-"+data["property[document]"];
            var phone = data["country_code"]+"-"+data["phone"];

            $(this).request('Registration::onAjax', {
                data:{
                    'phone' : phone,
                    'property': {
                        'document': document_number
                    }
                       
                },
                success: function(data){
                    if(data.status == false){
                        if (data.message.endsWith("is busy")) {
                            translation = translator.get("busy");
                            toastr.warning(translation, 'Espera' );
                        }else{
                            toastr.warning(translator.get(data.message), 'Espera' );
                        }
                        
                    }else{
                        this.success(data);
                    }
                    

    
                }
            });
        }
    });

    $("body").on("click", ".logout", function(){
        $.request('Logout::onAjax').then(function(){
            location.reload();
        });
    });

    $("#profile-info-container").on("submit", "#user-profile-info", function(e){
        e.preventDefault();
        var message = "";
        var form = $(this).serializeArray();

        var data = convertToObject(form);

        message = validateRegister(data);

        if (message == "") {
            var document_number = data["property[document_type]"]+"-"+data["property[document]"];
            var phone = data["country_code"]+"-"+data["phone"];
            
            $(this).request('UserPage::onAjax', {
                
                    
                'update': {
                    'profile/user-data': '#profile-info-container'
                },
                data:{
                    'phone' : phone,
                    'property': {
                        'document': document_number
                    }
                       
                },
                success: function(response){
                    if (response.status == true) {
                        toastr.success("Tu información ha sido modficad correctamente", 'OK' );
                        
                        
                    }else{
                        toastr.warning(translator.get(response.message), 'Espera' );
                    }
                    this.success(response);
                    console.log(response);
                },
                error: function(response){
                    console.log(response);
                }
            });
        
        }else{
            toastr.warning(translator.get(message), 'Espera' );
        }
        

    });

    $("#change-password").on("submit", function(e){
        e.preventDefault();
        var message = "";
        var form = $(this).serializeArray();
        //console.log(data);
        var data = convertToObject(form);

        var message = validateChangePassword(data);
        if (message =="") {

            $(this).request('ChangePassword::onAjax', {
                success: function(response){
                    if (response.status == true) {
                        toastr.success("Tu contraseña ha sido cambiada", 'OK' );
                        $("#change-password").trigger("reset");
                    }else{
                        toastr.warning(translator.get(response.message), 'Espera' );
                    }
                    console.log(response);
                },
                error: function(response){
                    console.log(response);
                }
            });
        }else{
            toastr.warning(message, 'Espera' );
        }
        
    });

    $("body").on("submit", "#restore-form", function(e){
        e.preventDefault(); 

        $(this).request('RestorePassword::onAjax',{

            success: function(response){
                if (response.status == true) {
                    swal({
                        title: '¡Correcto!',
                        text: 'En breve recibirás un correo con instrucciones para reestablecer tu cuenta',
                        button:true,
                      });
                }else{
                    swal({
                        icon: 'warning',
                        title: '¡UPS!',
                        text: 'No hemos localizado tu correo en nuestra base de datos, verificalo',
                        button:true,
                    });
                }
                
            },
            error: function(response){
                swal({
                    icon: 'warning',
                    title: '¡UPS!',
                    text: 'No hemos localizado tu correo en nuestra base de datos, verificalo',
                    button:true,
                });
            }
        });
    });

    $("body").on("submit", "#reset-form", function(e){
        e.preventDefault();
        var message = "";
        var form = $(this).serializeArray();
        //console.log(data);
        var data = convertToObject(form);

        var message = validateChangePassword(data);
        if (message =="") {
            $(this).request('ResetPassword::onAjax',{

                success: function(response){
                    if (response.status == true) {
                        swal({
                            title: '¡Correcto!',
                            text: 'Has modificado tu contraseña correctamente, ahora puedes iniciar sesión',
                            button:true,
                          }).then(()=>{
                            location.href = "https://contigo24.net";
                          });
                    }else{
                        swal({
                            icon: 'warning',
                            title: '¡UPS!',
                            text: 'Ha ocurrido un error:'+response.message,
                            button:true,
                        });
                    }
                    
                },
                error: function(response){
                    swal({
                        icon: 'warning',
                        title: '¡UPS!',
                        text: 'Ha ocurrido un error, intenta nuevamente o comunicate con nosotros al chat',
                        button:true,
                    });
                }
            });
        }else{
            toastr.warning(message, 'Espera' );
        }

        
    });
});


function validateRegister(data){
    var errorMessage ="";

    if(data["name"]===""){
        errorMessage+="*Tu nombre es obligatorio <br>";
    }
    if(data["last_name"]===""){
        errorMessage+="*Tu apellido es obligatorio <br>";
    }
    if(data["email"]===""){
        errorMessage+="*Tu email es obligatorio <br>";
    }

    if(data["phone"]==="" && data["phone"].length < 7){
        errorMessage+="*Tu telefono es obligatorio y debe tener al menos 8 caracteres <br>";
    }
    
    if(data["property[document_type]"] == ""){
        errorMessage+="*Elige un tipo de documento<br>";
    }

    if(data["property[document]"] =="" || data["property[document]"].lenght <4 || (/^[0-9]*$/g).test(data["property[document]"]) == false){
        errorMessage+="*Tu documento debe tener al menos 4 caracteres y ser sólo números<br>";
    }
         
    return errorMessage;
}

function validateChangePassword(data){
    var errorMessage ="";

    if(data["old_password"]===""){
        errorMessage+="*Escribe tu contraseña anterior <br>";
    }
    if(data["password"]===""){
        errorMessage+="*Tu nueva contraseña es requerida <br>";
    }

    if(data["password_confirmation"]==="" || data["password_confirmation"] !== data["password"]){
        errorMessage+="*la confirmación de contraseña es requerida y debe ser igual al a nueva contraseña <br>";
    }


    return errorMessage;
}

    
