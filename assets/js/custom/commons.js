$(document).on('ajaxSetup', function(event, context) {
    // Enable AJAX handling of Flash messages on all AJAX requests
    context.options.flash = true

    // Enable the StripeLoadIndicator on all AJAX requests
    context.options.loading = $.oc.stripeLoadIndicator

    // Handle Error Messages by triggering a flashMsg of type error
    context.options.handleErrorMessage = function(message) {
        $.oc.flashMsg({ text: message, class: 'error' })
    }

    // Handle Flash Messages by triggering a flashMsg of the message type
    context.options.handleFlashMessage = function(message, type) {
        $.oc.flashMsg({ text: message, class: type })
    }
});

function convertToObject(array){
    var obj = {};
    array.forEach(element => {
        obj[element.name]= element.value;
        
    });
    return obj;
}

function initWizard(){
    $('#smartwizard').smartWizard({
        selected: 0,
        anchorClickable: false,
        enableURLhash: false,
        lang: { // Language variables for button
          next: 'Siguiente',
          previous: 'Anterior'
      }
    });

    $("#smartwizard").on("leaveStep", function(e, anchorObject, currentStepIndex, nextStepIndex, stepDirection) {
    
        

        var options = {
            errorSteps: [currentStepIndex+1],
        };
        if (stepDirection == "forward") {
            if (!validateStep(currentStepIndex)) {
                return false;
            }else{
                if (nextStepIndex == 3) {
                    $(".sw-btn-next").addClass("d-none");
                }else{
                    $(".sw-btn-next").removeClass("d-none");
                }
                return true;
            }
        }else{
            if (nextStepIndex == 3) {
                $(".sw-btn-next").addClass("d-none");
            }else{
                $(".sw-btn-next").removeClass("d-none");
            }
            return true;
        }
    });

     
}

function updateCollapses(){
    $('[data-collapse]').each(function (i, element) {
      const collapse = element;

      $('[data-collapse-trigger]', collapse).on('click', function () {
          const openedClass = $(this).closest('[data-collapse-opened-class]').data('collapse-opened-class');
          const item = $(this).closest('[data-collapse-item]');
          const content = item.children('[data-collapse-content]');
          const itemParents = item.parents();

          itemParents.slice(0, itemParents.index(collapse) + 1).filter('[data-collapse-item]').css('height', '');

          if (item.is('.' + openedClass)) {
              const startHeight = content.height();

              content.css('height', startHeight + 'px');
              item.removeClass(openedClass);

              content.height(); // force reflow
              content.css('height', '');
          } else {
              const startHeight = content.height();

              item.addClass(openedClass);

              const endHeight = content.height();

              content.css('height', startHeight + 'px');
              content.height(); // force reflow
              content.css('height', endHeight + 'px');
          }
      });

      $('[data-collapse-content]', collapse).on('transitionend', function (event) {
          if (event.originalEvent.propertyName === 'height') {
              $(this).css('height', '');
          }
      });
  });
  }

function validateStep(index){
    var form = $("#order-form").serializeArray();
    var data = convertToObject(form);

    var messages = "";
    console.log(index);
    switch (index) {
        case 0:
            messages = validateClientInfo(data);
            break;
        case 1: 
            messages = validateShippingType(data);
            break;
        case 2:
            messages = validateShippingInfo(data);
            break;
        case 3:
           message = validateContactInfo(data);
        default:
            break;
    }

    if (messages !="" ) {
        toastr.warning(messages, 'Espera' );
        return false;
    }else{
        return true;
    }

  }

  function validateClientInfo(data){
    var errorMessage = "";
   
    if(data["name"]===""){
        errorMessage+="*Tu nombre es obligatorio <br>";
    }
    if(data["last_name"]===""){
        errorMessage+="*Tu apellido es obligatorio <br>";
    }
    if(data["email"]===""){
        errorMessage+="*Tu email es obligatorio <br>";
    }

    if(data["phone"]==="" && data["phone"].length < 7){
        errorMessage+="*Tu telefono es obligatorio y debe tener al menos 8 caracteres <br>";
    }
    
    if(data["property[document_type]"] == ""){
        errorMessage+="*Elige un tipo de documento<br>";
    }

    if(data["property[document]"] =="" || data["property[document]"].lenght <4 || (/^[0-9]*$/g).test(data["property[document]"]) == false){
        errorMessage+="*Tu documento debe tener al menos 4 caracteres y ser sólo números<br>";
    }
    return errorMessage;
  }

  function validateShippingInfo(data){
    var errorMessage = "";
  
    if (data["order[shipping_type_id]"] == "2"){
        if(data["order[property][shipping_city]"] == "-1"){
            errorMessage+="*Elige una ciudad válida<br>";
        }
    
        if(data["order[property][shipping_address1]"] =="" || data["order[property][shipping_address1]"].lenght <15){
            errorMessage+="*Tu dirección debe tener al menos 15 caracteres<br>";
        }
    }

    if(typeof data["order[property][shipping_time]"] === 'undefined' || data["order[property][shipping_time]"] ==""){
      errorMessage+="*Elige un horario válido para la fecha seleccionada <br>";
    }
    return errorMessage;
  }

  function validateShippingType(data){
    var errorMessage = "";
    if (typeof data["order[shipping_type_id]"]  === 'undefined' || data["order[shipping_type_id]"] < 1){
        errorMessage+="*Elige un tipo de entrega o retiro <br>";
    }

    return errorMessage;
  }

  function validateContactInfo(data){
    var errorMessage = "";
    if(data["order[property][contact_number]"].length != 7 || data["order[property][contact_number]"] === "" || (/^[0-9]*$/g).test(data["order[property][contact_number]"]) == false){
      errorMessage+="*Ingresa un teléfono de contacto válido (7 caracteres numéricos)<br>";
    }

    return errorMessage;
  }

  function validateCard(data){
    var errorMessage = "";
    
    if(data["card_number"]==="" || data["card_number"].length != 16 || !/^[0-9]+$/.test(data["card_number"])){
        errorMessage+="*Número de tarjeta es inválido<br>";
    }
    if (data["card_holder_name"]==="" || data["card_number"].length < 8) {
        errorMessage+="*Ingresa el titular de la tarjeta<br>";
    }

    if (data["cvv"]==="" || data["cvv"].length != 3) {
        errorMessage+="*CVV inválido<br>";
    }
    
    if (typeof data["terms"] === 'undefined') {
        errorMessage+="*Debes aceptar los términos y condiciones<br>";
    }

    return errorMessage;
  }

  function validateZelle(data){
    var errorMessage = "";

    if(typeof data["reference"] == "undefined" || data["reference"] =="" || data["reference"].lenght <4){

        errorMessage+="*Escribe el titular de la cuenta zelle";

    }
    
    return errorMessage;
  }

  function validatePaypal(data){
    var errorMessage = "";

    if(typeof data["reference"] == "undefined" || data["reference"] =="" || data["reference"].lenght <4){

        errorMessage+="*Escribe el titular de la cuenta paypal";

    }
    
    return errorMessage;
  }

  function validateBanplus(data){
    var errorMessage = "";

    if(typeof data["reference"] == "undefined" || data["reference"] =="" || data["reference"].lenght <4){

        errorMessage+="*Escribe el titular de la cuenta";

    }
    
    return errorMessage;
  }

