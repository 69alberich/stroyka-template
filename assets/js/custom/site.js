$("document").ready(function(){
  
  /** offcanva cart */
  const cart = $('.dropcart--style--offcanvas');

  if (cart.length === 0) {
      return;
  }

  var flagPayment = true;
  var autoCompleteFlag = true;

  function cartIsHidden() {
      return window.getComputedStyle(cart[0]).visibility === 'hidden';
  }
  function showScrollbar() {
    $('body').css('overflow', '');
    $('body').css('paddingRight', '');
  }
  function hideScrollbar() {
      const bodyWidth = $('body').width();
      $('body').css('overflow', 'hidden');
      $('body').css('paddingRight', ($('body').width() - bodyWidth) + 'px');
  }
  function open() {
      hideScrollbar();

      $('.dropcart--style--offcanvas').addClass('dropcart--open');
  }
  function close() {
      if (cartIsHidden()) {
          showScrollbar();
      }

      $('.dropcart--style--offcanvas').removeClass('dropcart--open');
  }
  

  $('body').on('click', '[data-open="offcanvas-cart"]', function(event){
    if (!event.cancelable) {
        return;
    }
    event.preventDefault();
    open();
  });

  

  $("body").on("click", '.dropcart__backdrop, .dropcart__close', function(){
    close();
  });
  

  /*
  $("body").on("click", '.dropcart__backdrop, .dropcart__close').on('click', function(){
    close();
  });*/

  $('body').on('transitionend', ".dropcart--style--offcanvas", function(event){
      if ($(".dropcart--style--offcanvas").is(event.target) && event.originalEvent.propertyName === 'visibility' && cartIsHidden()) {
          showScrollbar();
      }
  });


  /** end offcanva cart */

  /*
  // mobile search
  */

  $("body").on("click", '.indicator--mobile-search .indicator__button', function(){
    
    if ($('.mobile-header__search').is('.mobile-header__search--open')) {
      $('.mobile-header__search').removeClass('mobile-header__search--open');
    } else {
      $('.mobile-header__search').addClass('mobile-header__search--open');
      $('.mobile-header__search').find('input')[0].focus();
    }

    $('.mobile-header__search').find('.search__button--type--close').on('click', function() {
      $('.mobile-header__search').removeClass('mobile-header__search--open');
    });
    
  });

  
$("body").on("click", function(e){
  if (!$(e.target).closest('.indicator--mobile-search, .mobile-header__search').length) {
    $('.mobile-header__search').removeClass('mobile-header__search--open');
  }
});


  /*
  // end mobile search
  */
  if($("#clock").length > 0){
    
    var date = $("#created_at").data("date");
    
    date = date.replace("a. m.", "am");
    date = date.replace("p. m.", "pm");
    
    $("#clock").countdown(date, function(event) {
        $(this).html(event.strftime('%Hh %Mm %Ss'));
    });
  }

  $('.dropify').dropify({
    messages: {
        'default': 'arrastra o haz click aqui para adjuntar tu comprobante',
        'replace': 'arrastra y suelta tu comprobante para reemplazarlo',
        'remove':  'remover',
        'error':   'Algo ha salido mal'
    },
    error: {
        'fileSize': 'Este archivo es muy pesado ({{ value }} max).',
        'minWidth': 'La imagen es muy pequeña ({{ value }}}px min).',
        'maxWidth': 'La imagen es muy grande ({{ value }}}px max).',
        'minHeight': 'El largo de la imagen es muy pequeño ({{ value }}}px min).',
        'maxHeight': 'El largo de la imagen es muy pequeño ({{ value }}px max).',
        'imageFormat': 'El formato de la imagen no está permitido ({{ value }} solamente).'
    }
  });

   $("body").on("click", ".currency-option", function(){
  
      var value = $(this).data("value");
        
        $.request('CurrencyList::onSwitch', {

            data: {currency: value}
          }).then(function(){
              location.reload();
          });
   });

   $("body").on("click", ".product-trigger", function(e){
     e.preventDefault();
     var productId = $(this).data("product-id");
     var prevQuantity = $(this).data("quantity");
     
     var data ={
      'productId': productId,
      'prevQuantity': prevQuantity
    }
    $.request('ProductHandler::onCheckProduct', {
      'data': data,
      'update': {
          'modals/product-modal': '.modal-content',
      }
      }).then(()=>{
          $("#quickview-modal").modal("show");
          $('.input-number').customNumber();
          
          initHeader();
         // $('select').formSelect();
         // $('.js-range-slider').ionRangeSlider();
      });
   });

   

   $("body").on("submit", "#product-modal-form, #product-single-form", function(e){
    e.preventDefault();
    var form = $(this).serializeArray();
    var formData = convertToObject(form);
    var data = {
        'cart': [
            {'offer_id': formData.offer, 'quantity': formData.quantity}
        ],
        'shipping_type_id': 1
    };
    
    $.request('Cart::onAdd', {
        'data': data,
        'update': {
            'desktop-header': '#desktop-header-container',
            'cart' : '#cart-container',
            'cart/product-list': '#cart-product-list-container',
            'cart/zone-list': '#zones-list-container',
            'cart/cities-list': '#cities-list-container',
            'mobile-header': '#mobile-header-container',
        },
    }).then(function(){
      
      $("#quickview-modal").modal("hide");
      //open();
      $("#cities-select").trigger("change");
      updateCollapses();
     
    });
  });

  $("body").on("submit", ".product-card-form", function(e){
   
    e.preventDefault();
    var form = $(this).serializeArray();
    var formData = convertToObject(form);
    var data = {
        'cart': [
            {'offer_id': formData.offer, 'quantity': formData.quantity}
        ],
        'shipping_type_id': 1
    };
    
    $.request('Cart::onAdd', {
        'data': data,
        'update': {
            'desktop-header': '#desktop-header-container',
            'cart' : '#cart-container',
            'cart/product-list': '#cart-product-list-container',
            'cart/zone-list': '#zones-list-container',
            'cart/cities-list': '#cities-list-container',
            'mobile-header': '#mobile-header-container',
        },
    }).then(function(){
      initHeader();
      updateCollapses();
      toastr.info("Añadido al carrito", "Correcto", 
      {timeOut: 1500, positionClass: 'toast-bottom-left'});
      //$("#quickview-modal").modal("hide");
      //open();
      
    });
  });
  

  $("body").on("click", ".remove-from-cart", function(){
    var offerId = $(this).data("offer-id");
    var data = {
        'cart': [offerId],
        'shipping_type_id': 4
    };
    
    //Send ajax request and update cart items
    $.request('Cart::onRemove', {
        'data': data,
        'update': {
          'desktop-header': '#desktop-header-container',
          'mobile-header': '#mobile-header-container',
          'cart' : '#cart-container',
          'cart/product-list':'#cart-product-list-container'
      },
      success: function(response){
        this.success(response)
        
      }
    }).then(function(){

      open();
      initHeader();
      updateCollapses();

    });
  });

  $("body").on("click", ".quickview__close", function(){
    $("#quickview-modal").modal("hide");
  });


  $('#quickview-modal').on('hidden.bs.modal', function (e) {
    
    initHeader();
  });

  /* STEPS */
  
  initWizard();

  const wizardOptions = {
    selected: 0,
    anchorClickable: false,

  }

  

  $("body").on("change", ".shipping-type-check", function(){
      var value = $(this).val();
    if (value == 2) {
        $("#delivery-section").removeClass("d-none");
    }else{
        $("#delivery-section").addClass("d-none");
    }
  });

  $("body").on("change", "#cities-select", function(){
    var value = $(this).find("option:selected").data("index");
    
    $("#cities-list-container .shipping-location-description").addClass("d-none");

    $("#shipping-location-description-"+value).removeClass("d-none");

    if(value !== "-1" ){

        $(this).request('CityList::onSetCityIdPage', {
            data: {
                city: $(this).val()
            },
            'update': {
                'cart/zone-list': '#zones-list-container'
            }
        }).then(function () {
            //alert("completado");
        });
    }else{
        $("#zones-list-container").empty();
        
    }
  });

  $("body").on("submit", "#order-form", function(e){
    e.preventDefault();
    var message = "";
    //var  form = $(this).serializeArray();
    var form = $(this).serializeArray();
    var data = convertToObject(form);

    
    if(data["order[property][contact_number]"].length != 7 || data["order[property][contact_number]"] === "" || (/^[0-9]*$/g).test(data["order[property][contact_number]"]) == false){
        message+="*Ingresa un teléfono de contacto válido (7 caracteres numéricos)<br>";
    }
    if (message!="") {
        toastr.warning(message, 'Espera' );
        return false;
    }else{
        var contact_number = data["phone_code"]+data["order[property][contact_number]"];
        $(this).request('MakeOrder::onCreate', {
          data:{
              'order':{
                  'property': {
                      'contact_phone': contact_number
                  }
              }     
          },
          'update': {
            'desktop-header': '#desktop-header-container',
            'mobile-header': '#mobile-header-container',
            'cart' : '#cart-container',
            'cart/product-list':'#cart-product-list-container'
          },
          'success':  function(data){
            
            
            if(typeof data.data.key !== 'undefined'){
              $(location).attr('href', 'https://farmacialacorina.com/orden/'+data.data.key);
              
            }else{
                //$(location).attr('href', 'http://localhost/dictya/order/'+data.secret);
                swal({
                  title: "Espera",
                  text: "Algunos productos han cambiado su disponibilidad, revisa tu carrito y vuelve a intentar",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                });
            }
            this.success(data);
            
          },
          'error': function(data){
            console.log(data);
          },
          'complete': function(){
              
              //loaderModal.close();
          }
          /*
          if(typeof data.data.key !== 'undefined'){
                       // console.log(data);
                       window.location.href = "https://dcarnes.com.ve/orden/"+data.data.key;
                        
                    }else{
                        swal.close();
                        swal({
                            title: "Espera",
                            text: "Algunos productos han cambiado su disponibilidad, revisa tu carrito y vuelve a intentar",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                          });
                    }
                    this.success(data);
          
          */
      });  
    }    
   
  });

  $('#payment-form').on('submit', function(e){
       
    var request = $(this).payStripe();
    
    request.then(function(response){
    var respuesta = JSON.parse(response.result);
    //console.log("respuesta", respuesta);
    if(respuesta.status === "succeeded"){
        //abre aqui
        var alerta_stripe = swal({
          title: 'Hemos procesado su pago correctamente, ',
          text: 'Espere mientras registramos su compra',
          closeOnEsc:false,
          allowEnterKey: false,
          allowOutsideClick: false,
          button:true,
          timer: 3000,
          }).then( response => {
            var data = {
              reference: respuesta["reference"],
              method_id: 2,
              currency: 1,
              status_id: 1,
              order_status_id: 5,
            } 
            console.log(data);
            $.request('PaymentsHandler::onAddPayment', {
                data: data,
                success: function(response) {
                  swal({
                    title: '¡Proceso Completado!',
                    content: 'Gracias por realizar tu compra con nosotros',
                    
                    }).then(function(){
                        location.reload();
                    });
                  
                },
                error: function(response){
                  showError(response.message);
                }
            });
      });
        //termina aqui
        
    }else{
        console.log(respuesta);
        
        } 
    });
    e.preventDefault();
  });

  
 
  /**PAYMENT SUBMIT */
  $("#mercantil-form").on("submit", function(e){
    e.preventDefault();
    
    
    var form = $(this).serializeArray();
    var data = convertToObject(form);
    var message = "";
    
    message =  validateCard(data);

    
    if (message !="") {
      toastr.warning(message, 'Espera' );
    }else{
      $(this).request('MerchantHandler::onGetManagedToken', {
        data: {
          "exp_date": data["exp_month"]+data["exp_year"],
        },
        success: function(response){
          console.log(response);
          if(response.Description =="Approved"){
            swal({
              title: 'Hemos procesado su pago correctamente, ',
              text: 'Espere mientras registramos su compra',
              closeOnEsc:false,
              allowEnterKey: false,
              allowOutsideClick: false,
              button:true,
              timer: 3000,
              }).then( () => {
                var data = {
                  reference: response.TransactionId,
                  method_id: 6,
                  currency: 1,
                  status_id: 1,
                  order_status_id: 5,
                } 
                console.log(data);
                $.request('PaymentsHandler::onAddPayment', {
                    data: data,
                    success: function(response) {
                      swal({
                        title: '¡Proceso Completado!',
                        content: 'Gracias por realizar tu compra con nosotros',
                        
                        }).then(function(){
                            location.reload();
                        });
                      
                    },
                    error: function(response){
                      showError(response.message);
                    }
                });
          });
          }else{
            console.log("soy error");
            console.log(response);
            showError(response.Description);
          }
        },
        error: function(response){
          console.log(response);
          showError(response.Description);
        }
      });
    }

    //9999999999999999
    
    
  });

  $(".file-payment-form").on("submit", function(e){
    e.preventDefault();

    var form = $(this).serializeArray();
    var data = convertToObject(form);
    var formId = $(this).attr("id");
    
    var message = "";

  switch (formId) {
    case "zelle-form":
      message = validateZelle(data);
      break;
    case "paypal-form":
      message = validatePaypal(data);
    case "banplus-form":
      message = validateBanplus(data);
    default:
      break;
  }    

    if (message!="") {
      toastr.warning(message, 'Espera' );
    }else{
        if(flagPayment){
            
            flagPayment = false;
            
            $(this).request('PaymentsHandler::onAddPayment', {

                'success': function(response){
                    console.log(response);
                    
                    swal({
                        title: "¡Bien hecho!",
                        text: "Hemos tu notificación de pago",
                        icon: "success",
                        button: "aceptar",
                      }).then((value)=>{
                          location.reload();
                      });
                      
                },
                'error': function(response){
                    console.log(response);
                    showError("Intenta nuevamente");
                },
                'complete':function(response){
                    flagPayment = true;
                }
            });
        }else{
            console.log("subiendo");
        }
        
    }

  });

  /* END PAYMENT SUBMIT */


  $("body").on("click", "#switchCurrency", function(){

    $.request('OrdersHandler::onChangeCurrency', {
        success: function(response){
            location.reload();
        },
    });
  });

  $("body").on("keyup", ".search__input", function(){
    var value = $(this).val();
   
    var update = {};
    var form = "#search-form";
    if ($(this).hasClass("mobile-input")) {
      update = {
        "suggestions": "#suggestions-container-mobile",
      };
      
      form = "#search-form-mobile";
    }else{
      
      update = {
        "suggestions": "#suggestions-container",
      };
    }

    if(value.length > 2 && autoCompleteFlag == true ){
      autoCompleteFlag = false;
      $(form).request('SearchHandler::onSearch', {
        data : {
          'not_redirect': 0,
        },
          update : update,
  
      }).then(()=>{
          autoCompleteFlag = true;
          if ($(this).hasClass("mobile-input")) {
            $("#mobile-search-input-container").addClass(["search--suggestions-open", "search--has-suggestions"]);
          }else{
            $("#search-input-container").addClass(["search--suggestions-open", "search--has-suggestions"]);
          }
          
      });
    }
  });

  $("body").on("submit", ".search__form", function(e){
    e.preventDefault();
    $(this).request('SearchHandler::onSearch', {
      data : {
        'not_redirect': 1,
      }
      });
  });

  

  function showError(message){
    console.log("dentro de error:"+message);
    swal({
      icon: 'warning',
      title: '¡UPS, ha ocurrido algo inesperado!',
      text: 'Codigo de error:'+message,
      button:true,
    }).then(()=>{
      location.reload();
    });
  }

  $("body").on("change", "input[name='order[shipping_type_id]']", function(){
    //alert("sirvo");
    
    var value = $(this).val();

    var data = {
        'shipping_type_id': value,

      };
      $.request('Cart::onSaveData', {
        'data': data,
        'update': {
            'cart/shipping-date': '#shipping-dates-container',
            'cart/shipping-time': '#shipping-timeblock-container',
            }
      }).then(()=>{
        var date  = $("#shipping-date-select").val();
        updateTimeBlock(date);
        $("#cities-select").trigger("change");
      });

      if (value == 2) {
        $("#nightfree-message").removeClass("d-none");
      }else{
        $("#nightfree-message").addClass("d-none");
      }
  });
  
  $("body").on("change", "#shipping-date-select", function(){
    
    var date = $(this).val();
    updateTimeBlock(date);

  });
  
  //Send ajax request and update cart items
  

  function updateTimeBlock(date){
   
    var dateParts = date.split("-");

    var selectedDate = moment(dateParts[1]+"-"+dateParts[0]+"-"+dateParts[2], "MM-DD-YYYY");
    var veCurrDate = moment().tz("America/Caracas");
    
    

    if (selectedDate.utcOffset() !== -240) {
       calculateUTCHours(selectedDate, veCurrDate);
    }
    
    if(selectedDate.format("M-D-YYYY") == veCurrDate.format("M-D-YYYY")){

      $("#shippingTime option").each(function(index, value){

        var lastIndex = ($("#shippingTime option").length) - 1;
        
        //console.log(lastIndex);

        if($(value).val() !== ""){

          var hourInterval = $(value).val().split("-");
          var timeInterval = hourInterval[1].split(":");//antes 1

          var auxDate = moment();
          //var auxDate = moment(hourInterval[1], "");
          auxDate.tz('America/Caracas');
          
          //auxDate.utc();
          auxDate.set('hour', timeInterval[0]);
          auxDate.set('minute', timeInterval[1]);
          auxDate.set('second', 0);

          calculateUTCHours(selectedDate, auxDate);

          if (index != lastIndex) {
            if(veCurrDate.isBefore(auxDate.subtract(2, "h"))){
             // console.log("si puedo");
            }else{
              $(value).prop("disabled", true);
             // console.log("no puedo");
            }
          }else{
            if(veCurrDate.isBefore(auxDate.subtract(1, "h"))){//antes 2
             // console.log("si puedo");
            }else{
              $(value).prop("disabled", true);
              //console.log("no puedo");
            }
          }
        }
      
      });
    }else{
      //console.log("no es hoy");
      $("#shippingTime option").each(function(index, value){
        if($(value).val() !== ""){
          $(value).prop("disabled", false);
        }
        
      });
    }
  
    $("body #shippingTime").val("");
  }


  $("#subscribe-form").on("submit", function(e){
    e.preventDefault();
    $(this).request("formSubscribe::onAddSubscriber", {
        success: function(response){
          
          swal({
            title: '¡Gracias por suscribirte!',
            text: 'Ahora estarás al tanto de todos nuestros servicios',
            button:true,
          });
            
        },
        error: function(response){
           
          swal({
              icon: 'warning',
              title: '¡UPS!',
              text: 'Parece que ha ocurrido un error, intenta nuevamente',
              button:true,
          });
        }
    });
  });

  $("body").on("submit", ".filter-form", function(e){
    e.preventDefault();
    $(this).request("SearchHandler::onFilter");
  });

  $("body").on("click", ".indicator--trigger--click", function(e){
    $(this).addClass("indicator--display indicator--open");
    
  });

  $("body").on("click", ".login-trigger", function(e){
    e.preventDefault();
    
    if($('#desktop-site-header').css('display')=='none') {
      $("#login-menu-mobile").trigger("click");   
    }else{
      $("#login-menu").trigger("click");
    }
  });

  
  function calculateUTCHours(baseDate, originDate){
    var utcOffset = baseDate.utcOffset();
    var diff = (utcOffset*-1 + -240)*-1;
    //console.log("offset in calculate:"+utcOffset);
    if (Math.sign(diff) == -1) {
      //console.log("añado:"+diff/60 );
      originDate.add(diff/60, "h");
      
    }else{
      //console.log("resto:"+diff/60 );
      originDate.subtract(diff/60, "h");
    }
  }
  /* END STEP */

  /*
  
  // Form Wizard

$( document ).ready(function() {

    setTimeout(function () {

        $('#smartwizard').smartWizard({
            selected: 0,
            transitionEffect: 'fade',
            toolbarSettings: {
                toolbarPosition: 'none',
            }
        });

        // External Button Events
        $("#reset-btn").on("click", function () {
            // Reset wizard
            $('#smartwizard').smartWizard("reset");
            return true;
        });

        $("#prev-btn").on("click", function () {
            // Navigate previous
            $('#smartwizard').smartWizard("prev");
            return true;
        });

        $("#next-btn").on("click", function () {
            // Navigate next
            $('#smartwizard').smartWizard("next");
            return true;
        });


        $('#smartwizard2').smartWizard({
            selected: 0,
            transitionEffect: 'slide',
            toolbarSettings: {
                toolbarPosition: 'none',
            }
        });

        // External Button Events
        $("#reset-btn2").on("click", function () {
            // Reset wizard
            $('#smartwizard2').smartWizard("reset");
            return true;
        });

        $("#prev-btn2").on("click", function () {
            // Navigate previous
            $('#smartwizard2').smartWizard("prev");
            return true;
        });

        $("#next-btn2").on("click", function () {
            // Navigate next
            $('#smartwizard2').smartWizard("next");
            return true;
        });


        $('#smartwizard3').smartWizard({
            selected: 0,
            transitionEffect: 'fade',
            toolbarSettings: {
                toolbarPosition: 'none',
            }
        });

        // External Button Events
        $("#reset-btn22").on("click", function () {
            // Reset wizard
            $('#smartwizard3').smartWizard("reset");
            return true;
        });

        $("#prev-btn22").on("click", function () {
            // Navigate previous
            $('#smartwizard3').smartWizard("prev");
            return true;
        });

        $("#next-btn22").on("click", function () {
            // Navigate next
            $('#smartwizard3').smartWizard("next");
            return true;
        });
    }, 500);
});
  
  
  */
  $('body').on('click', '.topbar-dropdown__btn', function() {
    //alert("aqui");
    $(this).closest('.topbar-dropdown').toggleClass('topbar-dropdown--opened');
  });


  $("body").on("change", "#payment-select", function(){
    var value = $(this).val();
    
    $(".payment-card").addClass("d-none");

    $("#card-"+value).removeClass("d-none");

  });
  
});